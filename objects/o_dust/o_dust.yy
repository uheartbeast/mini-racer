{
    "id": "bfb607a7-f1af-4b9e-98c5-7aa94b187c6e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_dust",
    "eventList": [
        {
            "id": "34e414cd-0fd5-428a-a9ff-a5816bcab201",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bfb607a7-f1af-4b9e-98c5-7aa94b187c6e"
        },
        {
            "id": "126ae8f9-8fc3-4ad0-a591-f8c9a89bcab7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bfb607a7-f1af-4b9e-98c5-7aa94b187c6e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "71702b6d-5f90-4cbb-ad49-1cd2956f4112",
    "visible": true
}