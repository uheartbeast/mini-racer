{
    "id": "99a7d690-1d61-4535-aa3d-27964dff4af5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_racecar",
    "eventList": [
        {
            "id": "3f5d961f-5616-4f12-837d-d05e9a6df570",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "99a7d690-1d61-4535-aa3d-27964dff4af5"
        },
        {
            "id": "d443ca30-3244-4e32-a24d-067e5b703020",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "99a7d690-1d61-4535-aa3d-27964dff4af5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "85d80d1e-ff22-440f-aa43-2c28aad3cc7f",
    "visible": true
}