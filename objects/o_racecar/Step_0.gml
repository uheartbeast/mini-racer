var _direction = point_direction(x, y, mouse_x, mouse_y);
var _acceleration = .1;
var _top_speed = 2;

depth = -y;

if mouse_check_button(mb_left) {
	friction = 0;
	motion_add(_direction, _acceleration);
	speed = min(speed, _top_speed);
} else {
	friction = _acceleration;
}

if speed > 0 {
	var _turn_amount = angle_difference(image_angle, _direction) * _acceleration;
	image_angle -= _turn_amount;
	
	var _dust_x = x + lengthdir_x(6, image_angle+180);
	var _dust_y = y + lengthdir_y(6, image_angle+180);
	instance_create_layer(_dust_x+random_range(-4, 4), _dust_y+random_range(-4, 4), "Instances", o_dust);
}