{
    "id": "71702b6d-5f90-4cbb-ad49-1cd2956f4112",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_dust",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "350facee-98fa-410a-974c-c89038cc1f80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71702b6d-5f90-4cbb-ad49-1cd2956f4112",
            "compositeImage": {
                "id": "634e4fd4-05f9-4cd4-b821-a18dceda6431",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "350facee-98fa-410a-974c-c89038cc1f80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "791272aa-c6b8-4e94-9fb5-76e1fe1baf63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "350facee-98fa-410a-974c-c89038cc1f80",
                    "LayerId": "eab3fdea-3514-468f-8e7b-3ce3d59736fb"
                }
            ]
        },
        {
            "id": "64727979-9e8b-4fef-9afd-d3b581d80199",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71702b6d-5f90-4cbb-ad49-1cd2956f4112",
            "compositeImage": {
                "id": "eeb0cef2-5f26-4c70-abd7-d7fab11c1d61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64727979-9e8b-4fef-9afd-d3b581d80199",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "167cf1b8-7faf-4cbf-878c-d59eb28d7c11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64727979-9e8b-4fef-9afd-d3b581d80199",
                    "LayerId": "eab3fdea-3514-468f-8e7b-3ce3d59736fb"
                }
            ]
        },
        {
            "id": "f3fec3bd-cd4f-491a-9e7b-989125d9902c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71702b6d-5f90-4cbb-ad49-1cd2956f4112",
            "compositeImage": {
                "id": "dab83fbf-25f3-4a69-9198-ed7e8687518e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3fec3bd-cd4f-491a-9e7b-989125d9902c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1de257d2-f7f5-494b-9bc5-4c1074c70687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3fec3bd-cd4f-491a-9e7b-989125d9902c",
                    "LayerId": "eab3fdea-3514-468f-8e7b-3ce3d59736fb"
                }
            ]
        },
        {
            "id": "9f70a5b7-ac98-45c3-b461-a719e5752ebb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71702b6d-5f90-4cbb-ad49-1cd2956f4112",
            "compositeImage": {
                "id": "7e701f66-7537-4c1d-8ec8-bc4f873e5923",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f70a5b7-ac98-45c3-b461-a719e5752ebb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c75ce0a-388a-4476-a5bd-e46af4d31661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f70a5b7-ac98-45c3-b461-a719e5752ebb",
                    "LayerId": "eab3fdea-3514-468f-8e7b-3ce3d59736fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "eab3fdea-3514-468f-8e7b-3ce3d59736fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71702b6d-5f90-4cbb-ad49-1cd2956f4112",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}